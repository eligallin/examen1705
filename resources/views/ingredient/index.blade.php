
@extends('layouts.app')

@section('content')
<div class="container">


<h1>
    Vista de ingredients
</h1>

<a href="/ingredients/create">
Alta de ingredient
</a>

<table class="table">   

    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Type_id</th>
        <th>Type_name</th>
        <th></th>
    </tr>


@foreach ($ingredients as $ingredient)
    <tr>
        <td>{{ $ingredient->id }}</td>
        <td>{{ $ingredient->name }}</td>
        <td>{{ $ingredient->type_id}}</td>
        <td>{{ $ingredient->type->name }}</td>
        <td>
        <a href="/ingredients/{{ $ingredient->id }}">Ver</a>
        <a href="/ingredients/{{ $ingredient->id }}/edit">Actualizar</a>
        <a href="/ingredients/{{ $ingredient->id }}/remember">Recordar</a>

        <form action="/ingredients/{{ $ingredient->id }}" method="post">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
        <input type="submit" value="borrar">
        </form>


        </td>

    </tr>
@endforeach
</table>
{{ $ingredients->links() }}
</div>
@endsection
