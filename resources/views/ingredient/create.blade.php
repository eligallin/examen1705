@extends('layouts.app')

@section('content')
<div class="container">


<h1>
    Alta de ingredient
</h1>

<div class="form">
<form action="/ingredients" method="post">
    {{ csrf_field() }}

  

    <div class="form-group">
        <label>Nombre: </label>
        <input type="text" name="name" value="{{ old('name') }}">
        
    </div>

  


    <div>
        <select type="select" name="type_id" value="">
            @foreach ($types as $type)
            <option value="{{ $type->id }}">
                {{ $type->name }}
            </option>
            @endforeach                
        </select>        
    </div>

    <div class="form-group">
        <input type="submit" value="Guardar">
    </div>    
</form>
</div>
</div>
@endsection
