@extends('layouts.app')

@section('content')
<div class="container">


<h1>
    Detalle de pizza
</h1>

<ul>
    <li>ID: {{ $pizza->id }}</li>
    <li>Name: {{ $pizza->name }}</li>
    <li>User: {{$pizza->user->name }}</li>
    
</ul>


<h3>Ingredientes de la pizza actual</h3>
    <ol>
        @foreach ($pizza->ingredients as $ingredient)
        <li>{{ $ingredient->id }} - {{ $ingredient->name }}</li>
        @endforeach
    </ol>

</div>
@endsection
