<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert(['name' => 'Bases']);
        DB::table('types')->insert(['name' => 'Quesos']);
        DB::table('types')->insert(['name' => 'Verduras']);
        DB::table('types')->insert(['name' => 'Carnes']);
        DB::table('types')->insert(['name' => 'Pescados']);
        DB::table('types')->insert(['name' => 'Otros']);
    }
}
