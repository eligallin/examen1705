<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Pizza;
use App\User;
use App\Ingredient;


class PizzaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $pizzas = Pizza::paginate(5);

         //return $pizzas;
         return view('pizza.index', ['pizzas' => $pizzas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $pizza = Pizza::findOrFail($id);
        return view('pizza.show', ['pizza' => $pizza]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pizza = Pizza::findOrFail($id);
         $pizza->delete();

        // Solución 2
        // Study::destroy($id);

        return redirect('/pizzas');
    }


     public function ingredient($id)
    {
        $pizza = Pizza::with('ingredients')->findOrFail($id);
        // return $pizza;

        //devolver la lista de módulos
        // return $pizza->modules;

        //devolver el estudio con el array de modulos
        return view('pizza.ingredients', ['pizza' => $pizza]);
        // return $pizza;
    }

    
}
