<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    
    protected $fillable = [
        'name', 'type_id'
    ];

    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function pizzas()
    {
        return $this->belongsToMany('App\Pizza');
    }
}
